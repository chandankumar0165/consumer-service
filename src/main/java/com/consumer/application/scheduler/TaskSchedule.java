package com.consumer.application.scheduler;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.consumer.application.service.ExcelFileService;
import com.consumer.application.service.SendEmailService;

@Service
public class TaskSchedule {
	
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ExcelFileService excelFileService;
	@Autowired
	SendEmailService emailService;
	
	@Scheduled(cron = "0 0 19 * * ?")
	public void importToExcel() {
		HttpHeaders headers = new HttpHeaders() {{
	         String auth = "demo" + ":" + "passwd#123";
	         byte[] encodedAuth = Base64.encodeBase64( 
	            auth.getBytes(Charset.forName("US-ASCII")) );
	         String authHeader = "Basic " + new String( encodedAuth );
	         set( "Authorization", authHeader );
	      }};
	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      
		HttpEntity <String> entity = new HttpEntity<String>(headers);
		List bookList = restTemplate.exchange("http://localhost:9090/books", 
				HttpMethod.GET, entity, ArrayList.class).getBody();
		
		excelFileService.saveInExcel(bookList);
		
		try {
			emailService.sendmail("test.email.4myself@gmail.com",
					"PFA.",
					"This email has attachment",
					"/home/chandan/Documents/book/book.xlsx");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		 
	}

}
