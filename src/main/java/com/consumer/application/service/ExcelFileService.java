package com.consumer.application.service;

import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class ExcelFileService {

	public void saveInExcel  (List bookList) {
		try {
			Workbook workBook = new XSSFWorkbook();
			Sheet sheet = workBook.createSheet("Book");
			String[] columnHeading = {"Id","Title","Category","isbn","Number Of Pages",
					"Year","Author","Publisher","Release Date", "Price"};
			
			Font headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short)12);
			headerFont.setColor(IndexedColors.BLACK.index);
			
			CellStyle headerStyle = workBook.createCellStyle();
			headerStyle.setFont(headerFont);
			headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
			
			Row headerRow = sheet.createRow(0);
			for(int i=0;i<columnHeading.length;i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columnHeading[i]);
				cell.setCellStyle(headerStyle);
			}
			CreationHelper creationHelper = workBook.getCreationHelper();
			CellStyle dateStyle = workBook.createCellStyle();
			dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat("MM/dd/yyyy"));
			int rowCount = 1;
			for(Object book: bookList) {
				Row row = sheet.createRow(rowCount++);
				if(!(((Map)book).get("id")==null))
				row.createCell(0).setCellValue((Integer)((Map)book).get("id"));
				row.createCell(1).setCellValue((String)((Map)book).get("title"));
				row.createCell(2).setCellValue((String)((Map)book).get("category"));
				row.createCell(3).setCellValue((String)((Map)book).get("isbn"));
				
				if(!(((Map)book).get("numberOfPages")==null))
				row.createCell(4).setCellValue((Integer)((Map)book).get("numberOfPages"));
				row.createCell(5).setCellValue((String)((Map)book).get("year"));
				row.createCell(6).setCellValue((String)((Map)book).get("author"));
				row.createCell(7).setCellValue((String)((Map)book).get("publisher"));
				Cell releaseDateCell = row.createCell(8);
				releaseDateCell.setCellValue((String)((Map)book).get("releaseDate"));
				releaseDateCell.setCellStyle(dateStyle);
				
				if(!(((Map)book).get("price")==null))
				row.createCell(9).setCellValue((Double)((Map)book).get("price"));
			}
			for(int i=0;i<columnHeading.length;i++) {
				sheet.autoSizeColumn(i);
			}
			FileOutputStream fileOut = new FileOutputStream("/home/chandan/Documents/book/book.xlsx");
			workBook.write(fileOut);
			fileOut.close();
			workBook.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendEmail()
	{
		
	}
	
}
